from time import time

from authlib.specs.rfc6750 import BearerTokenValidator
from authlib.specs.rfc7009 import RevocationEndpoint
from pony.orm import (
    Database,
    Required, Optional, PrimaryKey, Set
)
from datetime import datetime
from pony.orm.ormtypes import LongStr, StrArray

from .oauth2mixin import ClientMixin, AuthorizationCodeMixin, TokenMixin


class OAuth2ClientMixin(ClientMixin):
    client_id = Required(str, 48)
    client_secret = Required(str, 120)
    issued_at = Required(int, default=time)
    expires_at = Required(int, default=0)

    redirect_uris = Required(StrArray, default=[])
    token_endpoint_auth_method = Optional(str, 48, default='client_secret_basic')
    grant_types = Required(StrArray, default=[])
    response_types = Required(StrArray, default=[]) # code token
    scope = Required(str, default='')

    client_name = Optional(str, 100, default='')
    client_uri = Optional(str, default='')
    logo_uri = Optional(str, default='')
    contacts = Optional(StrArray, default=[])
    tos_uri = Optional(str, default='')
    policy_uri = Optional(str, default='')
    jwks_uri = Optional(str, default='')
    i18n_metadata = Optional(str, default='')

    software_id = Optional(str, 36, default='')
    software_version = Optional(str, 48, default='')


class OAuth2AuthorizationCodeMixin(AuthorizationCodeMixin):
    code = Required(str, 120, unique=True)
    client_id = Optional(str, 48)
    redirect_uri = Optional(str, default='')
    response_type = Optional(str, default='')
    scope = Optional(str, default='')
    auth_time = Required(int, default=time)


class OAuth2TokenMixin(TokenMixin):
    client_id = Optional(str, 48)
    token_type = Optional(str, 40)
    access_token = Required(str, 255, unique=True)
    refresh_token = Optional(str, 255)
    scope = Optional(str, default='')
    revoked = Optional(bool, default=False)
    issued_at = Required(int, default=time)
    expires_in = Required(int, min_value=0, default=0)


def create_query_client_func(client_model):
    def query_client(client_id):
        return client_model.select(lambda o: o.client_id==client_id).get()
    return query_client


def create_save_token_func(token_model):
    def save_token(token, request):
        user_id = request.user.get_user_id() if request.user else None
        # TODO здесь точно отвалится
        tok = token_model(client_id=request.client.client_id,
                          user=user_id,
                          **token)
        tok.save()
    return save_token


def create_query_token_func(token_model):
    def query_token(token, token_type_hint, client):
        q = token_model.select(client_id=client.client_id, revoked=False)

        if token_type_hint == 'access_token':
            return q.select(lambda o: o.access_token==token).get()

        if token_type_hint == 'refresh_token':
            return q.select(lambda o: o.refresh_token==token).get()

        # without token_type_hint
        return q.select(lambda o: o.access_token==token).get() or q.select(lambda o: o.refresh_token==token).get()
    return query_token



def create_revocation_endpoint(token_model):
    query_token = create_query_token_func(token_model)

    class _RevocationEndpoint(RevocationEndpoint):
        def query_token(self, token, token_type_hint, client):
            return query_token(token, token_type_hint, client)

        def revoke_token(self, token):
            token.revoked = True
            token.save()

    return _RevocationEndpoint


def create_bearer_token_validator(token_model):
    class _BearerTokenValidator(BearerTokenValidator):
        def authenticate_token(self, token_string):
            return token_model.select(lambda o: o.access_token==token_string).get()

        def request_invalid(self, request):
            return False

        def token_revoked(self, token):
            return token.revoked

    return _BearerTokenValidator
