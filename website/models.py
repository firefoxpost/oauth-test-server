import time
from pony.orm import (
    Database,
    Required, Optional, PrimaryKey, Set
)

from .ponyengine import (
    OAuth2ClientMixin,
    OAuth2AuthorizationCodeMixin,
    OAuth2TokenMixin,
)

db = Database()


class User(db.Entity):
    id = PrimaryKey(int, auto=True)
    username = Required(str, 40, unique=True)
    client = Optional('OAuth2Client')
    authorizationcode = Optional('OAuth2AuthorizationCode')
    token = Optional('OAuth2Token')

    def __str__(self):
        return self.username

    def get_user_id(self):
        return self.id

    def check_password(self, password):
        return password == 'valid'


class OAuth2Client(db.Entity, OAuth2ClientMixin):
    id = PrimaryKey(int, auto=True)
    #user_id = db.Column(
    #    db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    user = Required('User', cascade_delete=True)
    


class OAuth2AuthorizationCode(db.Entity, OAuth2AuthorizationCodeMixin):
    id = PrimaryKey(int, auto=True)
    #user_id = db.Column(
    #    db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    user = Required('User', cascade_delete=True)


class OAuth2Token(db.Entity, OAuth2TokenMixin):
    id = PrimaryKey(int, auto=True)
    #user_id = db.Column(
    #    db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    user = Required('User', cascade_delete=True)

    def is_refresh_token_expired(self):
        expires_at = self.issued_at + self.expires_in * 2
        return expires_at < time.time()
